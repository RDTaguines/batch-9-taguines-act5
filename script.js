//ESSAY PART
//1.What is the difference between a comparison operator and an assignment operator?
// An assignment operator,  is used for assigning the value to a variable.
//A comparison operator, used for comparing two values.It does not change any values, but return the booleans true or false depending on whether the JavaScript expression evaluates as true or false.

//2.What is a Boolean? What does it represent?
//Boolean is a logical data type and represents the concepts of true and false.

//3.How is the += operator different than + operator?
// += Sums up left and right operand values and assigns the result to the left operand, while + operator adds two numeric operands.

//CODING PART
//1.Set two variables equal to two different numbers. Use a comparison operator to compare these two variables. Change one of their values by using the +=, -=, *=, or /= operator. Then, compare their values again.

let num1 = 5;
let num2 = 10;

console.log(num1 < num2);//true

console.log(num1 += 7);//12
console.log(num1 < num2);//false

//2. Try out the <= and >= operators.

console.log(num1 <= num2);//fasle
console.log(num1 >= num2);//true

//3.Try to use the !== operator.

console.log(num1 !== num2);//true

console.log(num2 -= 5);//5
console.log(num1 !== num2);//true










